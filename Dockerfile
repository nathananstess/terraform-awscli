FROM alpine:3.7
ENV TERRAFORM_VERSION=0.12.6

RUN apk update
RUN apk add ca-certificates coreutils grep unzip wget zip curl jq

################################
# Install Terraform
################################

# Download terraform for linux
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
         
# Unzip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Move to local bin
RUN mv terraform /usr/local/bin/
# Check that it's installed
RUN terraform --version 

################################
# Install AWS CLI
################################

# https://github.com/senseyeio/docker-alpine-aws-cli/blob/master/Dockerfile
RUN apk -Uuv add groff less python py-pip
RUN pip install awscli
RUN apk --purge -v del py-pip

# Clean up
RUN rm /var/cache/apk/*

WORKDIR /usr/src/working